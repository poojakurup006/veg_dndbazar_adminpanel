app.service('OfferService', function($http, $q,$rootScope,$window, $ionicLoading){
	this.getOfferDetails=function(details){
		var deferred = $q.defer();
		
		var req={
               method: 'GET',
               url: '/getOfferDetails',
               headers: {
                         'Content-Type': 'application/json',
                         'data': details
                        }
                        /*dataType : 'application/json',
                         data: details*/

            };
		$http(req)
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

});

app.service('AuthService', function ($rootScope, $http, $q,$timeout){
	this.doSignUp=function(details){
		var deferred = $q.defer();
		
		var req={
               method: 'POST',
               url: '/doSignUp',
               headers: {
                         'Content-Type': 'application/json',
                         'data':details
                                      
                         }
                        
             };
		$http(req)
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};
	this.recoverPasswordonMobile=function(details){
		var deferred = $q.defer();
		
		var req={
               method: 'GET',
               url: '/recoverPasswordonMobile',
               headers: {
                         'Content-Type': 'application/json',
                         'data':details
                                      
                         }
                        
             };
		$http(req)
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};
});

app.service('CartService', function (_,$rootScope, $http,$timeout, $q){
	var allCartItems=[];
   var self = this;
   $rootScope.sumofAll=!_.isUndefined(window.localStorage.lsallCartItems) ?
														getSumofAll(JSON.parse(window.localStorage.lsallCartItems)) : 0;

	this.getFruits =function () {
	var deferred = $q.defer();
            
           var req={method: 'GET', url: server_url+'/getFruits',
			               headers: {'Content-Type': 'application/json'}
       				};
					$http(req).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
							console.log(angular.toJson(res));
						    	deferred.reject(data);
						    	});
          return deferred.promise;
	};

	this.getVegetables =function () {
	var deferred = $q.defer();
            
           var req={method: 'GET', url: server_url+'/getVegetables',
			               headers: {'Content-Type': 'application/json'}
       				};
					$http(req).success(function (res) {
						        deferred.resolve(res);
						    }).error( function(data){
							console.log(angular.toJson(res));
						    	deferred.reject(data);
						    	});
          return deferred.promise;
	};

	self.addCartItem = function(cartItem) {
		
		var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
														JSON.parse(window.localStorage.lsallCartItems) : [];

		

		var existing_item = _.find(allCartItems, function(post){ return ((post.id == cartItem.id) && (post.name===cartItem.name)); });
		
		if(!existing_item){

			allCartItems.push({
				id: cartItem.id,
				name : cartItem.name,
				hindi_name : cartItem.hindi_name,
				quantity: cartItem.quantity,
				image :cartItem.image,
				price :cartItem.price,
				quan :cartItem.quan,
				unit :cartItem.unit
			});
			$rootScope.totalitems=allCartItems.length;
			$rootScope.sumofAll=getSumofAll(allCartItems);
		}
		else{
		 var updatedCartItems=	_.extend(_.findWhere(allCartItems, { id: cartItem.id,name:cartItem.name }), cartItem);
		$rootScope.sumofAll=getSumofAll(allCartItems);
		 console.log(JSON.stringify(updatedCartItems));
		}

		window.localStorage.lsallCartItems = JSON.stringify(allCartItems);
		localStorage.removeItem('offerdetails');
         localStorage.removeItem('discont');
         localStorage.removeItem('oldTotal');
         $rootScope.$broadcast('OfferChange', { message: "test" });
		$rootScope.$broadcast("new-cart-item");

	};
	self.CartChange= function () {
		 
		var deferred = $q.defer();
		 var products;
		
		getFruitPrices().then(function(res){
			 var fruitprices=res;
			 products=self.getCartItems();
			if(fruitprices && fruitprices.length>0 && products.length>0){
		       	 		
		 			var combined = _.map(products, function(base){
		                        return _.extend(base, _.findWhere( fruitprices, { id: base.id,name : base.name} ));
		          });
		 			products= combined;
		 			$rootScope.sumofAll=getSumofAll(products);
		 			window.localStorage.lsallCartItems = JSON.stringify(products);

		 			getvegetablePrices().then(function(res){ var vegprices=res;
		 	
		 	 
				 	if(vegprices && vegprices.length>0 && products.length>0){
				       	 		
				 				var vcombined = _.map(products, function(base){
				                        return _.extend(base, _.findWhere( vegprices, { id: base.id,name : base.name} ));
				          });
				 			products= vcombined;
				 			$rootScope.sumofAll=getSumofAll(products);
				 			window.localStorage.lsallCartItems = JSON.stringify(products);
				 			deferred.resolve(products);
				 		}
				 	},function(err){deferred.reject(err);console.log("There is an error ");}) ;
			}
		},function(err){ deferred.reject(err);console.log("There is an error ");}) ;
	
		   return deferred.promise;
		 	
		 	
	};	
	self.getListPrices = function(listName,listData){
		var deferred = $q.defer();
		 $timeout(function(){ 
		      
		      var pricereq={ method: 'GET',url: listName ,headers: { 'Content-Type': 'application/json',data:angular.toJson(window.localStorage.customernumber)}};
		      
		       console.log("pricelist=========================>"+angular.toJson(pricereq));
		       $http(pricereq).success(function (res) { var combined={};
		       	console.log("response=========================>"+angular.toJson(res));	
		       	if(res && res.length>0){
		       		console.log("response=========================>"+angular.toJson(res));
		       	 		//$scope.$emit('CartChange',{'priceList':res});
		 				  combined = _.map( listData, function(base){
		                        return _.extend(base, _.findWhere( res, { id: base.id,name : base.name} ));
		          });}
		       	deferred.resolve(combined); }).error( function(data){
console.log(JSON.stringify(data))
		       		deferred.reject(data); });
		    },100);  return deferred.promise; 
	};

function getSumofAll(allCartItems){
			
		var sumofAll=0;	
		for(i=0;i<allCartItems.length;i++){
				sumofAll=sumofAll + roundToTwo(allCartItems[i].quantity * allCartItems[i].price);
		}
			var oldTotal=sumofAll;
            localStorage.setItem('oldTotal', JSON.stringify(oldTotal));
		var offerdetails=angular.fromJson(window.localStorage.offerdetails);
		if (offerdetails && offerdetails.offer_criteria<sumofAll) {
			
			var discont=0; var total=sumofAll;
			if(offerdetails.offer_unit === 'rs'){
                discont=offerdetails.offer_amt;
                total=total-(offerdetails.offer_amt);
                localStorage.setItem('discont', JSON.stringify(discont));
            }
            else
            {
                discont=(total*(($scope.offerdetails.offer_amt)/100));
                total=total-(total*((offerdetails.offer_amt)/100)).toFixed(2);
                localStorage.setItem('discont', JSON.stringify(discont));
            }
            sumofAll=total;
		}
		if ( sumofAll < 0 || sumofAll == 0) {
            localStorage.removeItem('offerdetails');
            localStorage.removeItem('discont');
            localStorage.removeItem('oldTotal');
            $rootScope.$broadcast('OfferChange', { message: "test" });
            sumofAll=0;
        }
        if (offerdetails && offerdetails.offer_criteria>sumofAll) {
        	localStorage.removeItem('offerdetails');
            localStorage.removeItem('discont');
            localStorage.removeItem('oldTotal');
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
		
    return Math.ceil(sumofAll);
}
function roundToTwo(num) {    
    			return +(Math.round(num + "e+2")  + "e-2");
		}
self.checkExistingItem = function(cartItem) {
	var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
		
		var existing_item = _.find(allCartItems, function(post){ console.log((post.id == cartItem.id) && (post.name===cartItem));return ((post.id == cartItem.id) && (post.name===cartItem.name)); });
		if(existing_item){return existing_item;}
		else {return;}
};

	self.removeCartItem = function(remove_item) {
		var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
													/*alert(allCartItems.length);*/
		 var status = _.reject(allCartItems, function(allCartItem) {

                return allCartItem.id == remove_item.id && allCartItem.name == remove_item.name ;
            });
		 
		 $rootScope.totalitems= status.length; // allCartItems.length;
		 $rootScope.sumofAll=getSumofAll(status);
		 localStorage.removeItem('offerdetails');
         localStorage.removeItem('discont');
        localStorage.removeItem('oldTotal');
         $rootScope.$broadcast('OfferChange', { message: "test" });
		window.localStorage.lsallCartItems = JSON.stringify(status);
	};

	self.getCartItems = function(posts) {
		var allCartItems =!_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
				return allCartItems;
		
	};
	getFruitPrices =function(){ 
      var deferred = $q.defer();
       $timeout(function(){ 
		      
   //var SERVER_URL='http://localhost:3000';
       pricereq={ method: 'GET',url: '/getFruitPrices',headers: {'Content-Type': 'application/json',data:window.localStorage.customernumber}};
       $http(pricereq).success(function (res) {   deferred.resolve(res);}).error( function(data){deferred.reject(data); });
       },100);
       return deferred.promise; 
    };
    getvegetablePrices =function(){ 
      var deferred = $q.defer();
   	$timeout(function(){ 
       pricereq={ method: 'GET',url: '/getVegetablePrices',headers: {'Content-Type': 'application/json',data:window.localStorage.customernumber}};
       $http(pricereq).success(function (res) {   deferred.resolve(res); }).error( function(data){deferred.reject(data); }); 
        },100);return deferred.promise; 
    };

    self.UpdateOrder=function(allproducts,mobile){
		//var allproducts =;
		
        var offerdet=JSON.parse(localStorage.getItem('offerdetails'));
        var couponcode=(offerdet!==null) ? (offerdet.offercode) : 'No Coupon';
        var oldTotal=(localStorage.getItem('oldTotal')!==null) ? JSON.parse(localStorage.getItem('oldTotal')) : $rootScope.sumofAll;
        var id=localStorage.getItem('orderid');

		var alldata={'id':id,'order_detail':JSON.stringify(allproducts),'coupon_code':couponcode , 'total_before_coupon_apply':oldTotal , 'total' : $rootScope.sumofAll};
		var deferred = $q.defer();
		console.log("all order data" + JSON.stringify(alldata));
		var req={method: 'GET', url:'/updateOrder',params: {data:JSON.stringify(alldata)}};
					$http(req).then(function(res) {
						       console.log("inside service success" + angular.toJson(res));
						       deferred.resolve(res);
						       },function(err){
						       	console.log("inside service error" + angular.toJson(err));
						       	deferred.reject(err);
						    	});
            //}, 1000);
          return deferred.promise;
	};


	self.ConfirmOrder=function(allproducts,user,deliveryTime){
		//var allproducts =;
		var deferred = $q.defer();
        var offerdet=JSON.parse(localStorage.getItem('offerdetails'));
        var couponcode=(offerdet!==null) ? (offerdet.offercode) : 'No Coupon';
        var oldTotal=(localStorage.getItem('oldTotal')!==null) ? JSON.parse(localStorage.getItem('oldTotal')) : $rootScope.sumofAll;

		var alldata={'order_detail':JSON.stringify(allproducts),'mobile':user.mobile , 'coupon_code':couponcode , 'total_before_coupon_apply':oldTotal , 'total' : $rootScope.sumofAll, 'delivery_time': deliveryTime ,'status':'pending','customer_details':JSON.stringify(user)};
		var deferred = $q.defer();
		console.log("all order data from service " + JSON.stringify(alldata).length);
        var req={method: 'GET', url:'/confirmOrder',params: {data:JSON.stringify(alldata)}};
					$http(req).then(function(res){
						       console.log("inside service success" + angular.toJson(res));
						       deferred.resolve(res);
						       },function(err){
						       	console.log("inside service error" + angular.toJson(err));
						       	deferred.reject(err);
						    	});
            //}, 1000);
          return deferred.promise;
	};

	self.getDeliveryTime=function(){
		var deferred = $q.defer();
        var req={method: 'GET', url: '/getDeliveryTime',headers: {'Content-Type': 'application/json',data:window.localStorage.customernumber}};
		$http(req).success(function (res) { deferred.resolve(res);        
						    }).error( function(data){deferred.reject(data);	});
          return deferred.promise;
	};
	self.getSupplierPrice =function (alldata,listData) {
	var deferred = $q.defer();
            
           var req={method: 'GET', url: '/GetSupplierPrice',params:{ data:angular.toJson(alldata)}};
					$http(req).success(function (res) {
						 var combined={};
		       			if(res && res.length>0){
		 				 combined = _.map( listData, function(base){
		                        return _.extend(base, _.findWhere( res, { id: base.id,name : base.name} ));
		          		});}
		       			deferred.resolve(combined);
						    }).error( function(data){
						    	deferred.reject(data);
						    	});
          return deferred.promise;
	};
});