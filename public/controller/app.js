var app= angular.module("underscore", []).factory("_", function() {
    return window._
}),
app=angular.module('Myapp', ['ionic','ng-sweet-alert','ngRoute','ngFileSaver','ui.grid','ui.grid.selection', 'ui.grid.exporter','ui.mask','underscore','ngDialog']);
app.config(function($httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
});

 app.run( ['$rootScope', '$location', '$http',
    function ($rootScope, $location, $http) {
    	//alert("test login1");
        // keep user logged in after page refresh

        //$rootScope.globals = $cookieStore.get('globals') || {};
        $rootScope.globals = angular.fromJson(localStorage.globals) || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
                     
        }
 	
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	//alert("test login2");
            // redirect to login page if not logged in and trying to access a restricted page
            setTimeout(function() {
                      $rootScope.menuData=  localStorage.testMenudata || {}; 
                 //alert("hello" + $rootScope.menuData);
                 //angular.element('#topMenu').html($rootScope.menuData);
                 //alert(angular.toJson(angular.element('#topMenu').text()))
          },1000);
            

            var restrictedPage = $.inArray($location.path(), ['/login', '/forgetPassword']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }
 
 ]);


app.config(function($routeProvider) {
	$routeProvider
	.when('/',{
		templateUrl: 'views/dashboard/home.html',
    controller:'homeController'
	})
  .when('/forgetPassword',{
    templateUrl: 'views/forget-password/forgetPassword.html',
    controller:'ForgetPasswordController'
  })

  .when('/feature/:id',{
    templateUrl: 'views/feature/feature.html',
    controller:'featureController'
  })
  .when('/staff',{ 
		templateUrl: 'views/staff/staff.html',
		controller:'showStaffcontroller'
	})
	.when('/viewlead',{
		templateUrl: 'views/viewlead/view_lead.html',
		controller:'leadFollowupcontroller as followup'
	})
  .when('/vegDetails/:name',{
    templateUrl:'views/vegdetails/vegDetails.html',
    controller : 'vegdetailscontroller'
  })
  .when('/masterVegData/:name',{
    templateUrl:'views/mastervegdetails/masterVegData.html',
    controller : 'mastervegdatacontroller'
  })  
  .when('/delivery',{
    templateUrl:'views/deliveryBoy/delivery.html',
    controller : 'deliveryController'
  }) 
  .when('/password',{
    templateUrl:'views/password/password.html',
    controller : 'ResetPasswordController'
  }) 
  .when('/addSCA',{
    templateUrl:'views/add_state_city_area/add_st_ct_ar.html',
    controller : 'addSCAController'
  })  
  .when('/deliverytime',{
    templateUrl:'views/chnge_delivery_time/delivery_time.html',
    controller : 'deliverytimeController'
  }) 
  .when('/adminRegistration',{
    templateUrl:'views/admin_registration/adminRegistration.html',
    controller : 'adminRegistrationController'
  })  
  .when('/ordereditpanel',{
    templateUrl:'views/order_edit_panel/order_edit.html',
    controller : 'OrderEditController'
  })   
  .when('/placeOrder',{
    templateUrl:'views/place_order/placeOrder.html',
    controller : 'PlaceOrderController'
  })
  .when('/recharge',{
    templateUrl:'views/rechrge/rechrgemaster.html',
    controller : 'rechrgeController'
  }) 
  .when('/balance',{
    templateUrl:'views/balance/balancemaster.html',
    controller : 'balanceController'
  })
  .when('/customerDetails',{
    templateUrl:'views/customer-details/customer_details.html',
    controller : 'customerDetailController'
  }) 
  .when('/notification',{
    templateUrl:'views/notification/notice.html',
    controller : 'gcmNoticeController'
  })
	.when('/login',{ 
		templateUrl: 'views/login/login.html',
		controller: 'LoginController'
	})
  .when('/logout',{ 
    templateUrl: 'views/login/login.html',
    controller: 'LogoutController'
  })
	 .when('/setcredentials/:id',{
    templateUrl: 'views/staff/setuserpass.html',
    controller: 'credentialsController'
  })      
	
});